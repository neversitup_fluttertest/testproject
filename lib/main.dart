import 'package:flutter/material.dart';
import 'package:neversituptest/page/homepage.dart';
import 'package:neversituptest/provider/homepageProvider.dart';
import 'package:neversituptest/provider/main_provider.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MainProvider(child: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(

        colorScheme: ColorScheme.fromSeed(seedColor: Colors.teal),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Currency'),
    );
  }
}



