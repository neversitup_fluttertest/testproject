import 'package:flutter/foundation.dart';

enum LoadingStatus { init, loading, loaded }

class LoadingProvider with ChangeNotifier {
  LoadingStatus status = LoadingStatus.init;

  loading() {
    status = LoadingStatus.loading;
    notifyListeners();
  }

  loaded() {
    status = LoadingStatus.loaded;
    notifyListeners();
  }
}
