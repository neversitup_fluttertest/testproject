import 'dart:async';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../model/curencyModel.dart';

class Api<T> {

  Map<String, String> _headerApi = {};

  Future<Response<T>> getRate(Object? body) async {
    dynamic _model;
    Map<String, dynamic>? _fail;
    String _url = 'https://api.coindesk.com/v1/bpi/currentprice.json';

    final response = await _httpConnection(_url, _headerApi, body);
    if (response.statusCode == 200) {
      debugPrint('statusCode : 200');
      _model = CurencyModel.fromJson(_jsonDecode(response.body));

    } else {
      debugPrint('Error : ${response.statusCode}');
      _fail = json.decode(response.body);
    }

    return Response<T>(_model!, _fail);
  }

  dynamic _jsonDecode(String json) {
    var resJson = jsonDecode(json);
    return resJson;
  }


  ///api environment
  Future<http.Response> _httpConnection(
      String url, Map<String, String>? headers, Object? body,
      {bool isJson = false}) async {
    if (body != null) {
      final response = headers != null
          ? http.post(Uri.parse(url),
              headers: headers, body: isJson ? jsonEncode(body) : body)
          : http.post(Uri.parse(url), body: isJson ? jsonEncode(body) : body);

      return response;
    } else {
      final response = headers != null
          ? http.get(Uri.parse(url), headers: headers)
          : http.get(Uri.parse(url));

      return response;
    }
  }

}

class Response<T> {
  T? success;
  Map<String, dynamic>? fail;

  Response(this.success, this.fail);
}
