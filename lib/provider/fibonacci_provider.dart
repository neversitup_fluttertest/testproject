import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class FibonacciProvider with ChangeNotifier {
  bool validate = false;
  late List<int> fiboList = [];
  late List<int> primenumList = [];
  late List<int> arrayList = [];
  List arr1 = [9, 3, 1, 4, 6, 7, 2, 1];
  List arr2 = [5, 3, 9, 7, 4, 6];

  late TextEditingController Textcontroller;
  late TextEditingController TextPrimecontroller;
  late TextEditingController TextArraycontroller;
  late TextEditingController TextPincontroller;

  fiboTest(n) async {
    int numb = int.parse(n);
    fiboList = [];

    for (int i = 0; i < numb; i++) {
      if (i == 0) {
        fiboList.add(0);
      } else if (i == 1) {
        fiboList.add(1);
      } else {
        fiboList.add(fiboList[i - 1] + fiboList[i - 2]);
      }
    }

    notifyListeners();
  }

  primenumtest(n) {
    int numb = int.parse(n);
    primenumList = [];
    for (var i = 2; primenumList.length < numb; i++) {
      if (i == 2 || i == 3 || i == 5 || i == 7) {
        primenumList.add(i);
      } else {
        if (i % 2 == 0 || i % 3 == 0 || i % 5 == 0 || i % 7 == 0) {
          print(false);
        } else {
          print(true);
          primenumList.add(i);
        }
      }
    }
    notifyListeners();
  }

  arraytest() {
    arrayList = [];

    for (var i = 0; i < arr1.length; i++) {
      for (var j = 0; j < arr2.length; j++) {
        if (arr1[i] == arr2[j]) {
          arrayList.add(arr2[j]);
        }
      }
    }

    notifyListeners();
  }

  validate1(String v) {
    if (v.length < 6) {
      validate = false;
    } else {
      validate = true;
    }
    notifyListeners();
  }

  validate2(String v) {
    if (v.contains('000')) {
      validate = false;
    } else if (v.contains('111')) {
      validate = false;
    } else if (v.contains('222')) {
      validate = false;
    } else if (v.contains('333')) {
      validate = false;
    } else if (v.contains('444')) {
      validate = false;
    } else if (v.contains('555')) {
      validate = false;
    } else if (v.contains('666')) {
      validate = false;
    } else if (v.contains('777')) {
      validate = false;
    } else if (v.contains('888')) {
      validate = false;
    } else if (v.contains('999')) {
      validate = false;
    } else {
      validate = true;
    }
    notifyListeners();
  }

  validate3(String v) {
    if (v.contains('012')) {
      validate = false;
    } else if (v.contains('123')) {
      validate = false;
    } else if (v.contains('234')) {
      validate = false;
    } else if (v.contains('345')) {
      validate = false;
    } else if (v.contains('456')) {
      validate = false;
    } else if (v.contains('567')) {
      validate = false;
    } else if (v.contains('678')) {
      validate = false;
    } else if (v.contains('789')) {
      validate = false;
    } else {
      validate = true;
    }
    notifyListeners();
  }

  validate4(String v) {
    int count = 0;

    if (v.contains('00')) {
      count++;
    }
    if (v.contains('11')) {
      count++;
    }
    if (v.contains('22')) {
      count++;
    }
    if (v.contains('33')) {
      count++;
    }
    if (v.contains('44')) {
      count++;
    }
    if (v.contains('55')) {
      count++;
    }
    if (v.contains('66')) {
      count++;
    }
    if (v.contains('77')) {
      count++;
    }
    if (v.contains('88')) {
      count++;
    }
    if (v.contains('99')) {
      count++;
    }

    print(count);
    if (count > 2) {
      validate = false;
    } else {
      validate = true;
    }
    notifyListeners();
  }
}
