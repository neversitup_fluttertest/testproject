
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'fibonacci_provider.dart';
import 'homepageProvider.dart';
import 'loading_provider.dart';



class MainProvider extends StatelessWidget {
  final Widget child;

  const MainProvider({
    Key? key,
    required this.child,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context)=> GetPriceProvider()),
        ChangeNotifierProvider(create: (context)=> FibonacciProvider()),
        ChangeNotifierProvider(create: (context)=> LoadingProvider()),

      ],
      child: child,
    );
  }
}