// import 'package:shared_preferences/shared_preferences.dart';
//
// class BitcoinDataProvider extends ChangeNotifier {
//   BitcoinData? _bitcoinData;
//   BitcoinData? get bitcoinData => _bitcoinData;
//
//   Future<void> saveBitcoinData(BitcoinData data) async {
//     _bitcoinData = data;
//     SharedPreferences prefs = await SharedPreferences.getInstance();
//     await prefs.setString('bitcoinData', json.encode(data.toJson()));
//     notifyListeners();
//   }
//
//   Future<void> loadBitcoinData() async {
//     SharedPreferences prefs = await SharedPreferences.getInstance();
//     String? bitcoinDataString = prefs.getString('bitcoinData');
//     if (bitcoinDataString != null) {
//       _bitcoinData = BitcoinData.fromJson(json.decode(bitcoinDataString));
//     }
//     notifyListeners();
//   }
// }
