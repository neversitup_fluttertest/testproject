import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:neversituptest/page/fibonacci.dart';
import 'package:neversituptest/provider/fibonacci_provider.dart';
import 'package:provider/provider.dart';

import '../provider/homepageProvider.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late GetPriceProvider _rateProvider;
  Timer? timer;

  @override
  void initState() {
    _rateProvider = Provider.of<GetPriceProvider>(context, listen: false);
    _rateProvider.getRate(true);
    _rateProvider.Textcontroller = TextEditingController();
    // timer = Timer.periodic(Duration(minutes: 1), (Timer t) => _rateProvider.getRate(false));
    super.initState();
  }

  @override
  void dispose() {
    _rateProvider.dispose();
    super.dispose();
  }
Listview(type){
  late List<String> typeOfList ;
    if(type=='USD'){
      typeOfList=  _rateProvider.usdList;
    }
    else if(type=='GBP'){
      typeOfList=  _rateProvider.gbpList;
    }
    else if(type=='EUR'){
      typeOfList=  _rateProvider.eurList;
    }
  typeOfList = typeOfList.reversed.toList();
 return ListView.builder(
    scrollDirection: Axis.vertical,
    itemCount: typeOfList.length,
    // reverse: true,
    itemBuilder: (BuildContext context, int index) {
      final usdValue = typeOfList[index];
      return Padding(
        padding: const EdgeInsets.only(left: 25.0,right: 25),
        child: Container(
          height: 40,
          decoration: BoxDecoration(color:index%2==0?Colors.white:Colors.black12),
          child: Center(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: Text(usdValue,     style: TextStyle(
                    color:index%2==0?Colors.teal:Colors.teal,
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                      height: 1,
                    ),),
                  ),const Spacer(),
                 index==0? const Padding(
                   padding: EdgeInsets.only(right: 10.0),
                   child: Text('Latest',     style: TextStyle(
                     color:Colors.black38,
                     fontSize: 16,
                     fontWeight: FontWeight.w600,
                     height: 1,
                   ),),
                 ):
                 const Text('')

                ],
              ),
            ),
          ),
        ),
      );
    },
  );


}

DrawerList(){
    return Drawer(
      // Add a ListView to the drawer. This ensures the user can scroll
      // through the options in the drawer if there isn't enough vertical
      // space to fit everything.
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: [
          const DrawerHeader(
            decoration: BoxDecoration(
              color: Colors.teal,
            ),
            child: Text('Menu'),
          ),
          ListTile(
            title: const Text('Currency'),
            onTap: () {
              Navigator.push(
                  context, CupertinoPageRoute(builder: (_) =>  MyHomePage(title: 'Currentcy',)));
            },
          ),
          ListTile(
            title: const Text('Fibonacci'),
            onTap: () {
              Navigator.push(
                  context, CupertinoPageRoute(builder: (_) =>  FibonacciPage(title: 'Fibonacci',)));
            },
          ),


        ],
      ),
    );

}
  @override
  Widget build(BuildContext context) {
    return Consumer<GetPriceProvider>(
      builder: (context, provider, child) {
        return Scaffold(
          appBar: AppBar(
            backgroundColor: Theme.of(context).colorScheme.inversePrimary,
            title: Text(widget.title),
          ),
          drawer: DrawerList(),
          body: SingleChildScrollView(
            child: Container(
              color: Colors.transparent,
              child: SafeArea(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    const SizedBox(height: 20),
                    Padding(
                      padding: const EdgeInsets.only(left: 30.0, right: 30.0),
                      child: Container(
                        color: Colors.white,
                        child: TextField(
                          onChanged: (val) {
                            _rateProvider.calCulate(val);
                          },
                          inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                          obscureText: false,
                          keyboardType: TextInputType.number,
                          controller: _rateProvider.Textcontroller,
                          decoration: const InputDecoration(
                            labelText: 'Amount',
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(height: 20),
                    Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            ElevatedButton(
                              onPressed: () {
                                _rateProvider.typeChange('USD');
                              },
                              style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                  _rateProvider.type == 'USD' ? Colors.teal : Colors.white,
                                ),
                                textStyle: MaterialStateProperty.all(const TextStyle(fontSize: 30)),
                                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                ),
                              ),
                              child: Text(
                                'USD',
                                style: TextStyle(
                                  color: _rateProvider.type == 'USD' ? Colors.white : Colors.teal,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600,
                                  height: 1,
                                ),
                              ),
                            ),
                            ElevatedButton(
                              onPressed: () {
                                _rateProvider.typeChange('GBP');
                              },
                              style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                  _rateProvider.type == 'GBP' ? Colors.teal : Colors.white,
                                ),
                                textStyle: MaterialStateProperty.all(const TextStyle(fontSize: 30)),
                                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                ),
                              ),
                              child: Text(
                                'GBP',
                                style: TextStyle(
                                  color: _rateProvider.type == 'GBP' ? Colors.white : Colors.teal,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600,
                                  height: 1,
                                ),
                              ),
                            ),
                            ElevatedButton(
                              onPressed: () {
                                _rateProvider.typeChange('EUR');
                              },
                              style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                  _rateProvider.type == 'EUR' ? Colors.teal : Colors.white,
                                ),
                                textStyle: MaterialStateProperty.all(const TextStyle(fontSize: 30)),
                                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                ),
                              ),
                              child: Text(
                                'EUR',
                                style: TextStyle(
                                  color: _rateProvider.type == 'EUR' ? Colors.white : Colors.teal,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600,
                                  height: 1,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    const SizedBox(height: 20),
                    const Icon(
                      Icons.transform_rounded,
                      color: Colors.teal,
                      size: 36.0,
                    ),
                    const SizedBox(height: 20),
                    Padding(
                      padding: const EdgeInsets.only(left: 25.0, right: 25.0),
                      child: Container(
                        height: 80,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.white,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Padding(
                              padding: EdgeInsets.all(4.0),
                              child: Text(
                                'You will receive',
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 15,

                                  height: 1,
                                ),
                              ),
                            ),
                            const SizedBox(height: 20),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: [
                                  Text(
                                    _rateProvider.sum.toStringAsFixed(4) ,
                                    style: const TextStyle(
                                      color: Colors.black,
                                      fontSize: 15,
                                      fontWeight: FontWeight.w600,
                                      height: 1,
                                    ),
                                  ),
                                  const Spacer(),
                                  Container(
                                    width: 20,
                                    height: 20,
                                    decoration: const BoxDecoration(
                                      image: DecorationImage(
                                        fit: BoxFit.cover,
                                        image: NetworkImage(
                                          'https://cdn-icons-png.flaticon.com/512/5968/5968260.png',
                                        ),
                                      ),
                                    ),
                                  ),
                                  const SizedBox(width: 10),
                                  const Text(
                                    'BTC',
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 15,
                                      fontWeight: FontWeight.w600,
                                      height: 1,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(height: 20),
                    Padding(
                      padding: const EdgeInsets.only(right: 25.0),
                      child: Align(
                        alignment: Alignment.bottomRight,
                        child: Text(
                          '1 BTC = ${_rateProvider.RatePrice} ${_rateProvider.type}',
                          style: const TextStyle(
                            color: Colors.black26,
                            fontSize: 15,
                            fontWeight: FontWeight.w600,
                            height: 1,
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(height: 20),
                    Padding(
                      padding: const EdgeInsets.only(left: 25.0,right: 25),
                      child: Container(
                        height: 50,
                        width: double.infinity,

                        decoration: const BoxDecoration(
                          color: Colors.teal,
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(20.0),
                              topLeft: Radius.circular(20.0),
                        ),),
                        child: Center(child:  Text(
                          _rateProvider.type,
                          style: const TextStyle(
                            color:  Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w600,
                            height: 1,
                          ),
                        ),),
                      ),
                    ),
                    SizedBox(
                      height: 230,
                      child:  Listview(_rateProvider.type),
                    )

                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
