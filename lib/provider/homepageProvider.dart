import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:provider/src/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../model/curencyModel.dart';
import '../service/api.dart';
import 'loading_provider.dart';


class GetPriceProvider with ChangeNotifier {

  late List<String> usdList = [];
  late List<String> gbpList = [];
  late List<String> eurList = [];
  double sum = 0.0;
  String type = 'USD';
  double RatePrice = 0;
  CurencyModel? PriceModel;
  late TextEditingController Textcontroller;
  late double storedPrice;


  typeChange(typechange) async {
    notifyListeners();
    type = typechange;
    if (type == 'USD') {
      RatePrice = (PriceModel?.bpi?.uSD?.rateFloat ?? 0.00);
    }
    if (type == 'GBP') {
      RatePrice = (PriceModel?.bpi?.gBP?.rateFloat ?? 0.00);
    }
    if (type == 'EUR') {
      RatePrice = (PriceModel?.bpi?.eUR?.rateFloat ?? 0.000);
    }
    Textcontroller.clear();
    sum = 0.0;
    notifyListeners();
  }

  calCulate(val) async {
    sum = double.parse(val) / RatePrice;
    notifyListeners();
  }

  getRate(bool first) async {
    var api = Api<CurencyModel?>();
    var res = await api.getRate(null);

    if (res.fail == null) {
      PriceModel = res.success;

      if (type == 'USD') {
        RatePrice = (PriceModel?.bpi?.uSD?.rateFloat ?? 0.00);
      }
      if (type == 'GBP') {
        RatePrice = (PriceModel?.bpi?.gBP?.rateFloat ?? 0.00);
      }
      if (type == 'EUR') {
        RatePrice = (PriceModel?.bpi?.eUR?.rateFloat ?? 0.000);
      }
      SharedPreferences prefs = await SharedPreferences.getInstance();
      usdList = prefs.getStringList('usdList') ?? [];
      gbpList = prefs.getStringList('gbpList') ?? [];
      eurList = prefs.getStringList('eurList') ?? [];

      usdList.add(PriceModel?.bpi?.uSD?.rateFloat.toString() ?? '');
      gbpList.add(PriceModel?.bpi?.gBP?.rateFloat.toString() ?? '');
      eurList.add(PriceModel?.bpi?.eUR?.rateFloat.toString() ?? '');

      await prefs.setStringList("usdList", usdList);
      await prefs.setStringList("gbpList", gbpList);
      await prefs.setStringList("eurList", eurList);

      if (first) {
        typeChange('USD');
      }
      RatePrice;
      notifyListeners();
    } else {
      if (kDebugMode) {
        print(res.fail);
      }
    }
  }
}
