import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../provider/fibonacci_provider.dart';
import 'homepage.dart';

class FibonacciPage extends StatefulWidget {
  const FibonacciPage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<FibonacciPage> createState() => _FibonacciPageState();
}

class _FibonacciPageState extends State<FibonacciPage> {
  late FibonacciProvider fiboProvider;
  Timer? timer;

  @override
  void initState() {
    fiboProvider = Provider.of<FibonacciProvider>(context, listen: false);
    fiboProvider.Textcontroller = TextEditingController();
    fiboProvider.TextPrimecontroller = TextEditingController();
    fiboProvider.TextPincontroller = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    fiboProvider.dispose();
    super.dispose();
  }

  DrawerList() {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          const DrawerHeader(
            decoration: BoxDecoration(
              color: Colors.teal,
            ),
            child: Text('Menu'),
          ),
          ListTile(
            title: const Text('Currency'),
            onTap: () {
              Navigator.push(
                  context,
                  CupertinoPageRoute(
                      builder: (_) => const MyHomePage(
                            title: 'Currentcy',
                          )));
            },
          ),
          ListTile(
            title: const Text('Fibonacci'),
            onTap: () {
              Navigator.push(
                  context,
                  CupertinoPageRoute(
                      builder: (_) => const FibonacciPage(
                            title: 'Fibonacci',
                          )));
            },
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<FibonacciProvider>(
      builder: (context, provider, child) {
        return Scaffold(
          appBar: AppBar(
            backgroundColor: Theme.of(context).colorScheme.inversePrimary,
            title: Text(widget.title),
          ),
          drawer: DrawerList(),
          body: SingleChildScrollView(
            child: Container(
              color: Colors.transparent,
              child: SafeArea(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    const SizedBox(height: 20),
                    Padding(
                      padding: const EdgeInsets.only(left: 30.0, right: 30.0),
                      child: Container(
                        color: Colors.white,
                        child: TextField(
                          onChanged: (val) {
                            fiboProvider.fiboTest(val);
                          },
                          inputFormatters: [
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          obscureText: false,
                          keyboardType: TextInputType.number,
                          controller: fiboProvider.Textcontroller,
                          decoration: const InputDecoration(
                            labelText: 'Fibonacci',
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(height: 20),
                    Text(fiboProvider.fiboList.toString()),
                    const SizedBox(
                      height: 10,
                    ),
                    Container(
                      color: Colors.black38,
                      height: 2,
                      width: double.infinity,
                    ),
                    const SizedBox(height: 20),
                    Padding(
                      padding: const EdgeInsets.only(left: 30.0, right: 30.0),
                      child: Container(
                        color: Colors.white,
                        child: TextField(
                          onChanged: (val) {
                            if (val.isNotEmpty) {
                              fiboProvider.primenumtest(val);
                            }
                          },
                          inputFormatters: [
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          obscureText: false,
                          keyboardType: TextInputType.number,
                          controller: fiboProvider.TextPrimecontroller,
                          decoration: const InputDecoration(
                            labelText: 'Primenumber',
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(height: 20),
                    Text(fiboProvider.primenumList.toString()),
                    const SizedBox(
                      height: 10,
                    ),
                    Container(
                      color: Colors.black38,
                      height: 2,
                      width: double.infinity,
                    ),
                    const SizedBox(height: 20),
                    const Text(
                      'Array filter',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                        height: 1,
                      ),
                    ),
                    const SizedBox(height: 20),
                    Text(fiboProvider.arr1.toString()),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(fiboProvider.arr2.toString()),
                    const SizedBox(
                      height: 10,
                    ),
                    ElevatedButton(
                      onPressed: () {
                        fiboProvider.arraytest();
                      },
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(Colors.teal),
                        textStyle: MaterialStateProperty.all(
                            const TextStyle(fontSize: 30)),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                        ),
                      ),
                      child: Text(
                        fiboProvider.arrayList.toString(),
                        style: const TextStyle(
                          color: Colors.white,
                          fontSize: 15,
                          fontWeight: FontWeight.w600,
                          height: 1,
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Container(
                      color: Colors.black38,
                      height: 2,
                      width: double.infinity,
                    ),
                    const SizedBox(height: 20),
                    const Text(
                      'Pin Validate',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                        height: 1,
                      ),
                    ),
                    const SizedBox(height: 20),
                    Padding(
                      padding: const EdgeInsets.only(left: 30.0, right: 30.0),
                      child: Container(
                        color: Colors.white,
                        child: TextField(
                          onChanged: (val) {
                            if (val.isNotEmpty) {
                              print(val);
                              fiboProvider.validate1(val);
                              // fiboProvider.validate2(val);
                              // fiboProvider.validate3(val);
                              // fiboProvider.validate4(val);
                            }
                          },
                          inputFormatters: [
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          obscureText: false,
                          keyboardType: TextInputType.number,
                          controller: fiboProvider.TextPincontroller,
                          decoration: const InputDecoration(
                            labelText: 'Pinn',
                          ),
                        ),
                      ),
                    ),
                    ElevatedButton(
                      onPressed: () {},
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(
                            (fiboProvider.validate) ? Colors.teal : Colors.red),
                        textStyle: MaterialStateProperty.all(
                            const TextStyle(fontSize: 30)),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                        ),
                      ),
                      child: Text(
                        (fiboProvider.validate) ? 'O':'X',
                        style: const TextStyle(
                          color: Colors.white,
                          fontSize: 15,
                          fontWeight: FontWeight.w600,
                          height: 1,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
